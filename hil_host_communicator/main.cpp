/*
 * Title:: main.c
 * Brief:: Application for interpretting ros commands  
 * and sending them to the SIM board over UDP
 * Copyright: Stocked Robotics, All rights reserved
 * Author:: Nicholas Hemstreet
 */
 

// Includes directives
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <time.h> // Library for delay function
#include <pthread.h> // Threading library 
#include <semaphore.h>
#include <unistd.h>
#include <errno.h>
#include <chrono>
#include <sys/types.h>// For uint8_t type
#include "SocketClient.h" // Socket Type

// Namespace definition
using namespace std;

// Buffer Values
#define INPUTBUFFSIZE 512
#define OUTPUTBUFFSIZE 14


// Local IP Address
#define CONTROLLER_IP_ADDR "192.168.0.10"

// SIM IP Address
#define SIM_IP_ADDR "192.168.0.100"

// HIL IP Address
#define HIL_IP_ADDR "192.168.0.200"

// Socket Ports to connect to
#define SIM_PORT 4004 // UUT Port
#define HIL_PORT 4005 // HIL Firmware Port
#define GUI_PORT 5666 // Port for the GUI 
// Local Socket Ports
#define AI_PORT 1234 // Port for the controller to bind to
#define HIL_CONTROLLER_PORT 1235 // Port for the controller to bind to
#define HIL_GUI_PORT 1236  // Port for the HIL_GUI communicator to bind to

// Defined flag masks
#define BRAKE_MASK              0b0000000000000001
#define HORN_MASK               0b0000000000000010
#define FORK_UP_MASK            0b0000000000000100
#define FORK_DWN_MASK           0b0000000000001000
#define FORK_LEFT_MASK          0b0000000000010000
#define FORK_RIGHT_MASK         0b0000000000100000
#define FORK_FWD_MASK           0b0000000001000000
#define FORK_REV_MASK           0b0000000010000000
#define AUTO_MODE_MASK          0b0000000100000000
#define MANUAL_MODE_MASK        0b0000001000000000
#define EBRAKE_MODE_MASK        0b0000010000000000
#define FAIL_MODE_MASK          0b0000100000000000
#define SIXTY_DEGREE_MODE_MASK  0b0001000000000000
// Mask Macros
#define TURN_MASK_ON(reg,mask) reg |= mask;
#define TURN_PIN_MASK_OFF(reg,mask) (reg -= (reg&mask)*((reg&mask) == mask));
#define MASK_ON(reg,mask) ((reg& mask) == mask)

typedef struct SOCKET_DATA_TYPE
{
    uint8_t inputBuff[INPUTBUFFSIZE]; // Significantly larger because the SIM will be running much faster than us
    int freeDataIndex=0;
    uint8_t outputBuff[OUTPUTBUFFSIZE];    
}socket_data;

typedef struct HIL_CONTROLLER_PACKET_TYPE{
    // Throttle Goals
    short goalThrottlePercent;//!< Percent of throttle from -100 to 100
    // Steering Goals
    short goalSteeringAngle; //!< Angle scaled by 100, 157 == 1.57 rad

    // Goal Flags
    uint16_t goalFlags; //!< Flags we should be seeing
}hil_controller_packet;

typedef struct HIL_FRMWR_PACKET_TYPE{
    // Throttle Goals
    short goalThrottlePercent;//!< Percent from -100 to 100 
    short goalThrottleVoltage;//!< Voltage scaled by 100, 100 == 1.0 V
    
    // Steering Goals
    short goalSteeringAngle; //!< Angle scaled by 100, 157 == 1.57 rad
    short goalSteeringAVoltage;//!< Voltage scaled by 100, 100 = 1.0V
    short goalSteeringBVoltage;//!< Voltage scaled by 100, 100 = 1.0V

    // Goal Flags
    uint16_t goalFlags; //!< Flags we should be seeing

    // Read Throttle values
    short readThrottlePercent;//!< Throttle in percent
    short readThrottleVoltage;//!< Throttle Voltage

    // Read Steering Values
    short readSteeringAngle;//!< Angle read from the outputs
    short readSteeringAVoltage;//!< Read steering A voltage
    short readSteeringBVoltage;//!< Read steering B voltage
    // Read Flags
    uint16_t readFlags;//!< Flags read by the HAL
}hil_frmwr_packet;

typedef struct HIL_GUI_PACKET_TYPE{
    // Throttle Goals
    short goalThrottlePercent;//!< Percent from -100 to 100 
    short goalThrottleVoltage;//!< Voltage scaled by 100, 100 == 1.0 V
    
    // Steering Goals
    short goalSteeringAngle; //!< Angle scaled by 100, 157 == 1.57 rad
    short goalSteeringAVoltage;//!< Voltage scaled by 100, 100 = 1.0V
    short goalSteeringBVoltage;//!< Voltage scaled by 100, 100 = 1.0V

    // Goal Flags
    uint16_t goalFlags; //!< Flags we should be seeing

    // Read Throttle values
    short readThrottlePercent;//!< Throttle in percent
    short readThrottleVoltage;//!< Throttle Voltage

    // Read Steering Values
    short readSteeringAngle;//!< Angle read from the outputs
    short readSteeringAVoltage;//!< Read steering A voltage
    short readSteeringBVoltage;//!< Read steering B voltage
    // Read Flags
    uint16_t readFlags;//!< Flags read by the HAL
    uint64_t millisecondsSinceEpoch;    
}hil_gui_packet;

vector<hil_frmwr_packet> rxPackets_;

const struct timespec rxSleepTime_ = {0,500000};

const struct timespec txSleeptime_ = {0,500000};
pthread_t hilCommunicationThread;


int DecodeHILFirmwarePackets(vector<hil_frmwr_packet>& decodedPackets, uint8_t* buff, int buff_size){
    // if the buffer is not big enough for a packet return an error code
    if (buff_size < 33){
        return -1;
    }
    // Scan through the buffer looking for a header
    int index = 0;
    int end = buff_size-1;
    int foundHeaders = 0;
    int cutoffIndex = 0; // Last place we pulled data from.. will be the new start of the buffer
    while(index != end){
        if (buff[index] == 0xFF){
            // Found a header!
            foundHeaders++;
        }else{
            foundHeaders = 0;
        }
        // Found the beginning of a packet and we have enough data left to fill out that packet
        // Advance the index
        index++;
        if (foundHeaders == 5 && ((end-index) > 23)){
            foundHeaders = 0;
            
            hil_frmwr_packet pack;
            
            pack.goalThrottlePercent  = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.goalThrottleVoltage  = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.goalSteeringAngle    = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.goalSteeringAVoltage = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.goalSteeringBVoltage = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.goalFlags            = (uint16_t)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            
            pack.readThrottlePercent  = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.readThrottleVoltage  = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.readSteeringAngle    = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.readSteeringAVoltage = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.readSteeringBVoltage = (short)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            pack.readFlags            = (uint16_t)((buff[index+1]<<8) | (buff[index]));
            index += 2;
            
            cutoffIndex = index;
            decodedPackets.push_back(pack);
        }else if (foundHeaders == 5 && ((end-index) <= 25)){
            // This is the end of the data available in this buffer break and we will reset it.
            cutoffIndex = index; 
            break;
        }
    }
    if (cutoffIndex > end){
        cutoffIndex = end;
    }
    // Return the index of the last byte after the end of the last packet
    return cutoffIndex;
}

int EncodeFirmwarePacketForGui(hil_frmwr_packet pack,uint8_t* buff, int size){
    if (size < 37){
        return -1;
    }
    // do some vodoo ninja stuff to get the right time point
    for(int i=0; i < 5; i++){
        buff[i] = 0xFF;
    }
    std::chrono::system_clock::duration dtn = std::chrono::system_clock::now().time_since_epoch();
    
    uint64_t timestamp = dtn.count()/1000000;

    int index = 5;
    memcpy((uint8_t*)(buff+index),&pack.goalThrottlePercent,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.goalThrottleVoltage,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.goalSteeringAngle,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.goalSteeringAVoltage,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.goalSteeringBVoltage,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.goalFlags,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.readThrottlePercent,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.readThrottleVoltage,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.readSteeringAngle,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.readSteeringAVoltage,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.readSteeringBVoltage,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&pack.readFlags,2);
    index += 2;
    memcpy((uint8_t*)(buff+index),&timestamp,8);
    return 0;
}

int main (int argc, char** argv){
    
		// Create the data container for the incoming data
    vector<hil_frmwr_packet> decodedPackets;
    // Create the socket
    SocketClient hilSocket(CONTROLLER_IP_ADDR,HIL_CONTROLLER_PORT);
    if(!hilSocket.ConnectToClient(HIL_IP_ADDR,HIL_PORT)){
        cout << "Failed to connect to HIL... Make sure it is connected and restart the program" << endl;
        return -1;
    }
    SocketClient guiSocket(CONTROLLER_IP_ADDR,HIL_GUI_PORT);
    if(!guiSocket.ConnectToClient(CONTROLLER_IP_ADDR,GUI_PORT)){
        cout << "Failed to connect to the GUI socket... Make sure both are on" << endl;
        return -1;
    }
    // Create the buffer for the incoming data
    uint8_t recvdHILFirmwareData[512];
    uint8_t recvdHILSoftwareData[512];
    int recvdHILFirmwareStartFreeByteIndex = 0;
    // Enter the while forever loop
    while(1){
		    // Read data from the HIL
        if (hilSocket.DataAvailable() > 0 && hilSocket.DataAvailable() > (512-recvdHILFirmwareStartFreeByteIndex)){
            int readBytes = hilSocket.Recv((uint8_t*)(recvdHILFirmwareData+recvdHILFirmwareStartFreeByteIndex),(512-recvdHILFirmwareStartFreeByteIndex));    
            recvdHILFirmwareStartFreeByteIndex += readBytes;
        }else if (hilSocket.DataAvailable() > 0){
            int readBytes = hilSocket.Recv((uint8_t*)(recvdHILFirmwareData+recvdHILFirmwareStartFreeByteIndex),hilSocket.DataAvailable());
            recvdHILFirmwareStartFreeByteIndex += readBytes;
        }
        // Always loop through the whole buffer
        int newDataStart = DecodeHILFirmwarePackets(decodedPackets,recvdHILFirmwareData,512-recvdHILFirmwareStartFreeByteIndex);
        if (newDataStart == 511 || newDataStart == 0){
            // Empty the whole buffer
            memset(recvdHILFirmwareData,0,512);
            recvdHILFirmwareStartFreeByteIndex = 0;
        
        }else{
            // Copy up the values to the new value
            uint8_t temp[512];
            // Copy the left over parts from the recvdHILFirmwareData into temp
            // this is 511 not 512 because newDataStart is an index
            memcpy(temp,(uint8_t*)(recvdHILFirmwareData+newDataStart),511-newDataStart);
            memset(recvdHILFirmwareData,0,512);
            memcpy(recvdHILFirmwareData,temp,511-newDataStart);
        }

        // Handle re-transmission of the packets to the python GUI
        if (decodedPackets.size() > 0){
            // Load the new values into the rx vector
            for (vector<hil_frmwr_packet>::iterator it = decodedPackets.begin(); it != decodedPackets.end(); it++){
                rxPackets_.push_back(*it);
                uint8_t buff[41];
                hil_frmwr_packet packet = *it;
                // Print out the packet for debug
                cout << "goalThrottlePercent:: " << packet.goalThrottlePercent <<endl;
                cout << "goalThrottleVoltage:: " << packet.goalThrottleVoltage <<endl;
                cout << "goalSteeringAngle:: " << packet.goalSteeringAngle <<endl;
                cout << "goalSteeringAVoltage:: " << packet.goalSteeringAVoltage <<endl;
                cout << "goalSteeringBVoltage:: " << packet.goalSteeringBVoltage <<endl;
                cout << "goalFlags:: " << packet.goalFlags <<endl;

                cout << "readThrottlePercent:: " << packet.readThrottlePercent <<endl;
                cout << "readThrottleVoltage:: " << packet.readThrottleVoltage <<endl;
                cout << "readSteeringAngle:: " << packet.readSteeringAngle <<endl;
                cout << "readSteeringAVoltage:: " << packet.readSteeringAVoltage <<endl;
                cout << "readSteeringBVoltage:: " << packet.readSteeringBVoltage <<endl;
                cout << "readFlags:: " << packet.readFlags <<endl;

                // Encode and send these packets to the HIL GUI
                if(EncodeFirmwarePacketForGui((hil_frmwr_packet)packet,buff,41) != 0){
                    cout << "Error Encoding Firmware packet for the GUI" << endl;
                    pthread_exit(NULL);
                }
                if (guiSocket.Send(buff,37) != 37){
                    cout << "Failed to send encoded firmware packet to the GUI" << endl;
                }
            }
        }
        decodedPackets.clear();

        // Receive from the guiSocket and send it to the hil-firmware
        if (guiSocket.DataAvailable()){
            int readBytes;
            if (guiSocket.DataAvailable() < 512){
                readBytes = guiSocket.Recv(recvdHILSoftwareData,guiSocket.DataAvailable());
            }else{
                readBytes = guiSocket.Recv(recvdHILSoftwareData,510);
            }
            hilSocket.Send(recvdHILSoftwareData,readBytes);
        }
        // Give other threads the opportunity to run
        unsigned int usecs = 500;
        usleep(usecs);
    }
}
