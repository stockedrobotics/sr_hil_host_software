#include<iostream>
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<sys/ioctl.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "SocketClient.h"

SocketClient::SocketClient(std::string addr, int port){
    // Handle Error Cases
    if (addr == ""){
        std::cout << "No address given setting to 192.168.1.10" << std::endl;
        addr = "192.168.1.10";
    }
    
    if (port > 65536 || port < 0){
        std::cout<<"Invalid port given setting to 1234" << std::endl;
        port = 1234;
    }

    // Set home address and port
    this->home_addr = addr;
    this->home_port = port;
    
    // Create the home file descriptor
    this->home_fd = socket(AF_INET,SOCK_DGRAM,0);
    
    // Setup the descriptor
    this->home_sock_addr.sin_family = AF_INET;
    this->home_sock_addr.sin_port = htons(this->home_port);
    this->home_sock_addr.sin_addr.s_addr = inet_addr(this->home_addr.c_str());
    this->home_length = sizeof(this->home_sock_addr);
    this->initialized = false;

    if (bind(this->home_fd,(const struct sockaddr *) &this->home_sock_addr,this->home_length) != 0){
        std::cout << "Binding failed to addresss:: " << this->home_addr << std::endl;
    }
    std::cout << "Bound to " << addr << std::endl;
    std::cout << " At port:: " << this->home_port << std::endl;
}

bool SocketClient::ConnectToClient(std::string addr, int port){
    if (addr == ""){
        std::cout << "No address given setting to 192.168.1.100" << std::endl;
        addr = "192.168.1.100";
    }
    
    if (port > 65536 || port < 0){
        std::cout<<"Invalid port given setting to 4004" << std::endl;
        port = 4004;
    }
    this->dest_addr = addr;
    this->dest_port = (port);

    // Setup the descriptor
    this->dest_sock_addr.sin_family = AF_INET;
    this->dest_sock_addr.sin_port =htons(port);
    this->dest_sock_addr.sin_addr.s_addr = inet_addr(addr.c_str());
    this->dest_length = sizeof(this->dest_sock_addr);

    // Call the connect function
    if (connect(this->home_fd,(const struct sockaddr *)&this->dest_sock_addr,this->dest_length) != 0){
        std::cout << "Connection Failed" << std::endl;
        this->initialized = false;
    }else{
        std::cout << "Connection Success!" << std::endl;
        //std::cout << this->dest_sock_addr << std::endl;
        this->initialized = true;
    }
    return this->initialized;
}

int SocketClient::Send(unsigned char* buff, size_t buff_len){
    if (this->initialized){
        return (int)sendto(this->home_fd,buff,buff_len,0,(sockaddr *)&this->dest_sock_addr,this->dest_length);
    }else{
        return -1;
    }
}

int SocketClient::Recv(unsigned char* buff, size_t buff_len){
    if(this->initialized){
        // Return the amount of bytes written to this socket
        return (int) recvfrom(this->home_fd, buff,buff_len,0,(sockaddr *)&this->dest_sock_addr,&this->dest_length);
    }else{
        return -1;
    }
}

int SocketClient::DataAvailable(){
    int count;
    ioctl(this->home_fd, FIONREAD, &count);
    return count;
}

SocketClient::~SocketClient(){
    close(this->home_fd);
}