/*
 * Title:: SocketClient.h
 * Brief:: Header file for the SocketClient
 * Copyright: Stocked Robotics, All rights reserved
 * Date:: 07/04/2018
 * Author:: Nicholas Hemstreet
 */
 
#include<arpa/inet.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<string.h>
#include<stdlib.h>

#ifndef __SOCKET_CLIENT_H__
#define __SOCKET_CLIENT_H__

// C lib
//
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

class SocketClient
{
private:
    std::string home_addr;
    int home_port;
    std::string dest_addr;
    int dest_port;
    bool initialized;
    int home_fd;
    struct sockaddr home;
    struct sockaddr_in home_sock_addr;
    struct sockaddr_in dest_sock_addr;
    socklen_t home_length;
    socklen_t dest_length;
public:
    SocketClient(std::string addr, int port);
    bool ConnectToClient(std::string addr, int port);
    int Send(unsigned char* buff, size_t buff_len);
    int Recv(unsigned char* buff, size_t buff_len);
    int DataAvailable();

    ~SocketClient();
    
};

#endif // End of __SOCKET_CLIENT_H__