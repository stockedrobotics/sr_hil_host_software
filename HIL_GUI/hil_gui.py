"""
Title:: hil_gui.py
Brief:: Implementation of the hil_gui to be run alongside the hil_socket.cpp file
Author:: Nicholas Hemstreet
Copyright:: Stocked Robotics, All rights Reserved.s
"""

# Library includes
import sys
import os
import random
import matplotlib
import time
import multiprocessing as mp
import subprocess
import socket as sock
import struct
import cProfile,pstats,io
import csv
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets
from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

guiIpAddr_ = "192.168.0.10" ## IP Address for this GUI
localPort_ = 5666 ## Local Port for the GUI
intermediaryPort_ = 1236 ## Port that connects to the host_communicator
forkliftUnderTest_ = "Crown_PC4500" ## Vehicle Under testt

killRecordingFlag_ = mp.Value("i",1) ## flag for stopping the csv file recording
killCommunicationFlag_ = mp.Value("i",1) ## Flag for killing the socket connecting to the intermediary


class FirmwareToGUIPacket(object):
    """
    Helper class for loading new states and determining states based upon flags, interpretting given throttle percents to voltages and similarly with steering angles determining steering a and steering b voltages based upon radian values
    
    Encapsulates the packing and unpacking of data sent from the HIL_GUI to the host_communicator intermediary.
    """
    def __init__(self,forkliftType="Crown_PC4500"):
        """
        Initializes a FirmwareToGUIPacket Object

        Args:
            forkliftType: Forklift vehicle make to configure this gui for. Sets local variables that are necessary for proper implementaton see code for more details
        
        """

        super(FirmwareToGUIPacket, self).__init__()
        self.throttlePercentToVoltageGain = 0.0
        if (forkliftType == "Crown_PC4500"):
            # Offset taken form the firmware, needs to be updated based on the mapping
            self.throttlePercentToVoltageGain = 12.0/100.0
            # Offset taken form the firmware, needs to be updated based on the mapping
            self.steeringNeutralVoltage = 2.50
            self.steeringDeltaVoltage = 1.50
        elif (forkliftType == "Raymond_8510"):
            self.throttlePercentToVoltageGain = 12.0/100.0
            self.steeringNeutralVoltage = 3.5
            self.steeringDeltaVoltage = 0.5
        # There is no default case, we will just keep the above values
        self.goalThrottleVoltage = 0
        self.goalThrottlePercent = 0
        self.goalSteeringAngle = 0
        self.goalSteeringAVoltage = 0
        self.goalSteeringBVoltage = 0
        self.goalFlags = 0
        self.readThrottlePercent = 0
        self.readThrottleVoltage = 0
        self.readSteeringAngle = 0
        self.readSteeringAVoltage = 0
        self.readSteeringBVoltage = 0
        self.readFlags = 0
        self.timestamp = 0
        self.timestamp = 0.0
        # Flag Masks
        self.BrakeMask = 0b0000000000000001
        self.HornMask  = 0b0000000000000010
        self.Fork_Up_Mask = 0b0000000000000100
        self.Fork_Dwn_Mask = 0b0000000000001000
        self.Auto_Mode_Mask = 0b0000000100000000
        self.Manual_Mode_Mask = 0b0000001000000000
        self.EBrake_Mode_Mask = 0b0000010000000000
        self.Fail_Mode_Mask =   0b0000100000000000
        self.Sixty_Degree_Mode_Mask =  0b0001000000000000

    def getBrakeEngaged(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.BrakeMask) == self.BrakeMask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.BrakeMask) == self.BrakeMask):
                return True
            else:
                return False
    def getHornEngaged(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.HornMask) == self.HornMask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.HornMask) == self.HornMask):
                return True
            else:
                return False
    def getForksUp(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.Fork_Up_Mask) == self.Fork_Up_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Fork_Up_Mask) == self.Fork_Up_Mask):
                return True
            else:
                return False
    def getForksDwn(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.Fork_Dwn_Mask) == self.Fork_Dwn_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Fork_Dwn_Mask) == self.Fork_Dwn_Mask):
                return True
            else:
                return False
    def getAutoMode(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.Auto_Mode_Mask) == self.Auto_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Auto_Mode_Mask) == self.Auto_Mode_Mask):
                return True
            else:
                return False
    def getManualMode(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.Manual_Mode_Mask) == self.Manual_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Manual_Mode_Mask) == self.Manual_Mode_Mask):
                return True
            else:
                return False
    def getEBrakeMode(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.EBrake_Mode_Mask) == self.EBrake_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.EBrake_Mode_Mask) == self.EBrake_Mode_Mask):
                return True
            else:
                return False
    def getFailMode(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        if (readGoalFlags):
            if((self.goalFlags & self.Fail_Mode_Mask) == self.Fail_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Fail_Mode_Mask) == self.Fail_Mode_Mask):
                return True
            else:
                return False
    def getInSixtyDegreeDriveMode(self,readGoalFlags = 0):
        """
        Returns the state of either the goalFlags or the readFlags depending on the flag set
        
        Args: 
            readGoalFlags: If 0 we read the readFlags, if 1 we return the state of the goalFlags
        """
        
        if (readGoalFlags):
            if((self.goalFlags & self.Sixty_Degree_Mode_Mask) == self.Sixty_Degree_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Sixty_Degree_Mode_Mask) == self.Sixty_Degree_Mode_Mask):
                return True
            else:
                return False
    def setLatestTimestamp(self,timestamp):
        """
        Sets the latest timestamp to timestamp
        
        Args: 
            timestamp: timestamp to set the latest timestamp out of 
        """
        self.timestamp = timestamp

    def getLatestTimeStamp(self):
        """
        Returns the latest time stamp

        """
        return self.timestamp
    
    def setReadFlags(self,readFlags):
        """
        Set the read flags to the passed value

        Args: 
            readFlags: readFlags to be set
        """
        self.readFlags = readFlags

    def setGoalThrottle(self,percent):
        """
        Set the goal throttle to percent

        Args:
            percent: percent to set the goalThrottle to
        """
        self.goalThrottlePercent = percent
        self.goalThrottleVoltage = (self.throttlePercentToVoltageGain*abs(percent))

    def setGoalSteeringAngle(self,angle):
        """
        Set the goal steering angle to angle

        Args:
            angle: angle to set the steering angle to
        """

        self.goalSteeringAngle = angle
        self.goalSteeringAVoltage = self.steeringNeutralVoltage - (self.steeringDeltaVoltage*angle/90.0)
        self.goalSteeringBVoltage = self.steeringNeutralVoltage + (self.steeringDeltaVoltage*angle/90.0)
    
    def setBrakeEngaged(self,engaged):
        """
        Set the brake on or off
        
        Args:
            engaged: If true then the brake is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.BrakeMask
        else:
            self.goalFlags -= ((self.BrakeMask)*((self.goalFlags & self.BrakeMask) == self.BrakeMask))

    def setHornEngaged(self,engaged):
        """
        Set the horn on or off
        
        Args:
            engaged: If true then the horn is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.HornMask
        else:
            self.goalFlags -= ((self.HornMask)*((self.goalFlags & self.HornMask) == self.HornMask))
    
    def setForksUp(self,engaged):
        """
        Set the Forks Up on or off
        
        Args:
            engaged: If true then the Forks Up is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.Fork_Up_Mask
        else:
            self.goalFlags -= ((self.Fork_Up_Mask)*((self.goalFlags & self.Fork_Up_Mask) == self.Fork_Up_Mask))
    
    def setForksDwn(self,engaged):
        """
        Set the Forks Dwn on or off
        
        Args:
            engaged: If true then the Forks Dwn is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.Fork_Dwn_Mask
        else:
            self.goalFlags -= ((self.Fork_Dwn_Mask)*((self.goalFlags & self.Fork_Dwn_Mask) == self.Fork_Dwn_Mask))
    
    def setAutoMode(self,engaged):
        """
        Set the autoMode on or off
        
        Args:
            engaged: If true then the autoMode is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.Auto_Mode_Mask
        else:
            self.goalFlags -= ((self.Auto_Mode_Mask)*((self.goalFlags & self.Auto_Mode_Mask) == self.Auto_Mode_Mask))

    def setManualMode(self,engaged):
        """
        Set the Manual on or off
        
        Args:
            engaged: If true then the Manual is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.Manual_Mode_Mask
        else:
            self.goalFlags -= ((self.Manual_Mode_Mask)*((self.goalFlags & self.Manual_Mode_Mask) == self.Manual_Mode_Mask))

    def setEBrakeMode(self,engaged):
        """
        Set the E-Brake on or off
        
        Args:
            engaged: If true then the E-Brake is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.EBrake_Mode_Mask
        else:
            self.goalFlags -= ((self.EBrake_Mode_Mask)*((self.goalFlags & self.EBrake_Mode_Mask) == self.EBrake_Mode_Mask))
    
    def setFailMode(self,engaged):
        """
        Set the fail mode on or off
        
        Args:
            engaged: If true then the fail mode is turned on and if false off
        """
        if (engaged):
            self.goalFlags |= self.Fail_Mode_Mask
        else:
            self.goalFlags -= ((self.Fail_Mode_Mask)*((self.goalFlags & self.Fail_Mode_Mask) == self.Fail_Mode_Mask))
    
    def setInSixtyDegreeDriveMode(self,engaged):
        """
        Set the vehicle to sixty or ninety degree mode 
        
        Args:
            engaged: If true then the vehicle is in sixty degree mode, if false then it is in ninety degree mode
        """
        if (engaged):
            self.goalFlags |= self.Sixty_Degree_Mode_Mask
        else:
            self.goalFlags -= ((self.Sixty_Degree_Mode_Mask)*((self.goalFlags & self.Sixty_Degree_Mode_Mask) == self.Sixty_Degree_Mode_Mask))

    def getStateInByteFormat(self):
        """
        Return the format of the vehicle state in a byte string
        """
        byteStr = b""
        for i in range(0,5):
            byteStr += struct.pack("B",0xFF)

        byteStr += struct.pack("h",int(self.goalThrottlePercent))
        byteStr += struct.pack("h",int(self.goalSteeringAngle*(314.15/180)))
        byteStr += struct.pack("H",int(self.goalFlags))
        return byteStr


class MyDynamicMplCanvas(FigureCanvas):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, parent=None, width=5, height=4, dpi=100 ,title = "No Title Given", numberLines = 1):
        """
        Intiializes the dynamic Canvas

        Args:
            parent: Parent window to add this to
            width: The width of the graph
            height: The height of the grraph
            dpi: The dots per inch the graph will be drawn with
            title: Title for the Graph
            numberLines: The number of lines to be drawn on this graph
        """
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        
        self.axes = self.fig.add_subplot(111)

        # Initialize the figure
        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        
        self.timeData = [] # All time datas should be the same
        self.outputData = [] # data to graph
        self.numberLines = numberLines

        # Make all the arrays for appending data
        for i in range(0,numberLines):
            self.outputData.append([])

        ylim = self.axes.get_ylim()
        self.axes.cla()
        self.axes.set_ylim(ylim)
        self.draw()
    def getcolorForLineNbr(self,nbr):
        """
        Returns a colour for the line nbr

        Args:
            nbr: line to get the number for, only supports three currently
        """
        if(nbr == 0):
            return 'r'
        elif (nbr == 1):
            return 'g'
        elif (nbr == 2):
            return 'b'
        else:
            return 'p'

    def pushInValue(self, timeVal, dataVals):
        """
        Adds a new value to the graph and loads it based on fundamentals

        Args:
            timeVal: timeValue for the data
            dataVals: data to add to the values
        """
        if (len(self.outputData[0]) >= 5):
            # Pop On / Pop Off
            self.timeData.pop(0)
            self.timeData.append(timeVal)
            for i in range(0,self.numberLines):
                self.outputData[i].pop(0)
                self.outputData[i].append(dataVals[i])
        else:
            self.timeData.append(timeVal)
            for i in range(0,self.numberLines):
                self.outputData[i].append(dataVals[i])
            # Just add values

    def updateFigure(self):
        """
        Draws the graph and reloads the lines into the function
        """
        ylim = self.axes.get_ylim()
        self.axes.clear()
        for i in range(0,self.numberLines):
            self.axes.plot(self.timeData,self.outputData[i], self.getcolorForLineNbr(i))
        self.axes.set_ylim(ylim)
        self.draw()

class HilCommunicatorClass(object):
    """
    Class for communciating with the host_communciator
    """
    def __init__(self, communicationPipe):
        """
        Inititalizes a HilCommunciatorClass

        Args:
            communicationPipe: IPC pipe connected to the HILGui class for pushing data to after it is converted by the function
        """
        super(HilCommunicatorClass, self).__init__()
        # Initialize the communication socket
        self.socket = sock.socket(sock.AF_INET,sock.SOCK_DGRAM)
        self.socket.setblocking(False)
        self.socketBuff = b""
        self.commPipe = communicationPipe
        self.vehicleState = FirmwareToGUIPacket(forkliftUnderTest_)
        try:
            # bind ourselves to the HIL GUI Port
            self.socket.bind((guiIpAddr_,localPort_))
            # Connect to the C++ communication socket
            self.socket.connect((guiIpAddr_,intermediaryPort_))
            print("Connected!")
        except :
            print("Something went wrong while connecting to the C++ Socket. Please check it out")
            print("No Connection....")
            #sys.exit()
          
    def findPacketInBuffer(self):
        """
        Finds a packet in the buffer and converts it for you
        """
        foundHeaders = 0
        throttleData = []
        steeringData = []
        timeStamps = []
        lastFoundPacketIndex = -1
        for i in range(0,len(self.socketBuff)):
            if (self.socketBuff[i] == 0xFF):
                foundHeaders += 1
            else:
                foundHeaders = 0
            if (foundHeaders == 5 and(i+31) < len(self.socketBuff)-1):
                lastFoundPacketIndex = i+1
                foundHeaders = 0

        if (not lastFoundPacketIndex == -1):
            pipeDict = {}
            goalThrottlePercent = struct.unpack("h",self.socketBuff[lastFoundPacketIndex:lastFoundPacketIndex+2])[0]
            goalThrottleVoltage = struct.unpack("h",self.socketBuff[lastFoundPacketIndex+2:lastFoundPacketIndex+4])[0]
            goalSteeringAngle = struct.unpack("h",self.socketBuff[lastFoundPacketIndex+4:lastFoundPacketIndex+6])[0]
            goalSteeringAVoltage = struct.unpack("h",self.socketBuff[lastFoundPacketIndex+6:lastFoundPacketIndex+8])[0]
            goalSteeringBVoltage = struct.unpack("h",self.socketBuff[lastFoundPacketIndex+8:lastFoundPacketIndex+10])[0]
            goalFlags = struct.unpack("H",self.socketBuff[lastFoundPacketIndex+10:lastFoundPacketIndex+12])[0]
            
            readThrottlePercent  = struct.unpack("h",self.socketBuff[lastFoundPacketIndex+12:lastFoundPacketIndex+14])[0]
            readThrottleVoltage = struct.unpack("h",self.socketBuff[lastFoundPacketIndex+14:lastFoundPacketIndex+16])[0]
            readSteeringAngle  =  struct.unpack("h",self.socketBuff[lastFoundPacketIndex+16:lastFoundPacketIndex+18])[0]
            readSteeringAVoltage  =  struct.unpack("h",self.socketBuff[lastFoundPacketIndex+18:lastFoundPacketIndex+20])[0]
            readSteeringBVoltage  = struct.unpack("h",self.socketBuff[lastFoundPacketIndex+20:lastFoundPacketIndex+22])[0]
            readFlags = struct.unpack("H",self.socketBuff[lastFoundPacketIndex+22:lastFoundPacketIndex+24])[0]
            
            timestamp = struct.unpack("l",self.socketBuff[lastFoundPacketIndex+24:lastFoundPacketIndex+32])[0]
            # Eventually we may want to put things here for debugging whcih is why the goals are included in the packet, however they are not necessary at this stage so we will do nothing with them
            self.vehicleState.readThrottlePercent = readThrottlePercent # Stays the same
            self.vehicleState.readThrottleVoltage = readThrottleVoltage/100.0
            self.vehicleState.readSteeringAngle = (readSteeringAngle/100.0)*(180.0/3.1415)
            self.vehicleState.readSteeringAVoltage = readSteeringAVoltage/100.0
            self.vehicleState.readSteeringBVoltage = readSteeringBVoltage/100.0
            self.vehicleState.setReadFlags(readFlags)
            self.vehicleState.setLatestTimestamp(timestamp)

            pipeDict["readThrottlePercent"] = self.vehicleState.readThrottlePercent
            pipeDict["readThrottleVoltage"] = self.vehicleState.readThrottleVoltage
            pipeDict["readSteeringAngle"] = self.vehicleState.readSteeringAngle
            pipeDict["readSteeringAVoltage"] = self.vehicleState.readSteeringAVoltage
            pipeDict["readSteeringBVoltage"] = self.vehicleState.readSteeringBVoltage
            pipeDict["readFlags"] = self.vehicleState.readFlags
            pipeDict["timestamps"] =(timestamp)
            pipeDict["throttleData"] = (self.vehicleState.readThrottleVoltage,self.vehicleState.readThrottlePercent)
            pipeDict["steeringData"] = (self.vehicleState.readSteeringAngle,self.vehicleState.readSteeringAVoltage,self.vehicleState.readSteeringBVoltage)
            self.socketBuff = self.socketBuff[lastFoundPacketIndex+35:]
            self.commPipe.send(pipeDict)
        elif(len(self.socketBuff) > 256):
            self.socketBuff = b""
    
    def recvPacketFromGui(self):
        """
        Receive a packet from the gui
        """
        if (self.commPipe.poll(0)):
            vehSend = self.commPipe.recv()
            self.vehicleState.setGoalThrottle(vehSend["goalThrottlePercent"])
            self.vehicleState.goalThrottlePercent = vehSend["goalThrottlePercent"]
            self.vehicleState.setGoalSteeringAngle(vehSend["goalSteeringAngle"])
            self.vehicleState.goalFlags = vehSend["goalFlags"]
            try:
                byteStr = self.vehicleState.getStateInByteFormat()
                self.socket.sendto(byteStr,(guiIpAddr_,intermediaryPort_))
            except:
                pass
        else:
            pass
    def readDataFromSocket(self):
        """
        Read data from the socvket and convert the new data for the HIL Gui
        """
        # If there is data waiting on the socket then we read from it and pump it into our gui 
        addr = ("",-1)
        try:
            (data,addr) = self.socket.recvfrom(50)
        except:
            pass
        # If it is the C++ HIL Socket we receive the data
        if (addr == (guiIpAddr_,intermediaryPort_)):
            self.socketBuff += data
            graphData = self.findPacketInBuffer()


class HilGuiWindow(QtWidgets.QMainWindow):
    """
    GUI Class for the HI:
    """
    def __init__(self,communicationPipe):
        """
        Intiializes the HIL GUI with its parent as self

        Args:
            communcaitionPipe: pipe to the HILHostCommunciatorClass that handles the UDP socket
        """
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("Stocked Robotics HIL Testing Suite")
        self.testStartTime = time.time()

        self.file_menu = QtWidgets.QMenu('&File', self)
        self.file_menu.addAction('&SavePlots',self.savePlots)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
        
        self.vehicleState = FirmwareToGUIPacket(forkliftUnderTest_)

        self.commPipe = communicationPipe
        self.csvReceiverPipe = -1
        # Initialize timer for updating the gui
        self.updateTimer = QtCore.QTimer(self)
        self.updateTimer.timeout.connect(self.updateEvent)
        self.updateTimer.start(50)
        self.stateChanged = False

        self.sendTimer = QtCore.QTimer(self)
        self.sendTimer.timeout.connect(self.sendData)
        self.sendTimer.start(100)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)

        self.autoBagProcess = -1
        self.logDataProcess = -1
        self.csvWriterPipe = -1
        self.ciTestProcess = -1
        # Sent State Buttons
        self.sendBrakeStateButton = QtWidgets.QPushButton("Brake set to OFF")
        self.sendBrakeStateButton.clicked.connect(self.sendNewBrakeState)
        self.sendBrakeStateButton.setMinimumSize(200,20)
        self.sendBrakeStateButton.setAutoFillBackground(True)

        self.sendEBrakeStateButton = QtWidgets.QPushButton("E-Brake set to  OFF")
        self.sendEBrakeStateButton.clicked.connect(self.sendNewEBrakeState)
        self.sendEBrakeStateButton.setMinimumSize(200,20)
        self.sendEBrakeStateButton.setAutoFillBackground(True)
        
        self.sendForksUpStateButton = QtWidgets.QPushButton("Forks Up set to  OFF")
        self.sendForksUpStateButton.setMinimumSize(200,20)
        self.sendForksUpStateButton.clicked.connect(self.sendNewForksUpState)
        self.sendForksUpStateButton.setAutoFillBackground(True)
        
        self.sendForksDwnStateButton = QtWidgets.QPushButton("Forks Dwn set to  OFF")
        self.sendForksDwnStateButton.setMinimumSize(200,20)
        self.sendForksDwnStateButton.clicked.connect(self.sendNewForksDwnState)
        self.sendForksDwnStateButton.setAutoFillBackground(True)
        
        self.sendHornStateButton = QtWidgets.QPushButton("Horn set to  OFF")
        self.sendHornStateButton.setMinimumSize(200,20)
        self.sendHornStateButton.clicked.connect(self.sendNewHornState)
        self.sendHornStateButton.setAutoFillBackground(True)

        self.sendControlMode = QtWidgets.QPushButton("MANUAL MODE")
        self.sendControlMode.setMinimumSize(200,20)
        self.sendControlMode.setStyleSheet("background-color: #baf473")
        self.sendControlMode.clicked.connect(self.toggleControlMode)
        self.sendControlMode.setAutoFillBackground(True);
        
        self.driveModeButton = QtWidgets.QPushButton("Speed Mode Set to 90 Degree Mode",self)
        self.driveModeButton.setMinimumSize(200,20)
        self.driveModeButton.clicked.connect(self.toggleDriveMode)
        self.driveModeButton.setAutoFillBackground(True);

        # Slider Definitions
        self.sendThrottleSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal,self)
        self.sendThrottleSlider.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.sendThrottleSlider.setTickInterval(5)
        self.sendThrottleSlider.setSingleStep(1)
        self.sendThrottleSlider.setMinimum(-100)
        self.sendThrottleSlider.setMaximum(100)
        self.sendThrottleSlider.valueChanged.connect(self.throttleSliderValueChanged)

        self.sendSteeringAngleSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal,self)
        self.sendSteeringAngleSlider.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.sendSteeringAngleSlider.setTickInterval(5)
        self.sendSteeringAngleSlider.setSingleStep(1)
        self.sendSteeringAngleSlider.setMinimum(-90)
        self.sendSteeringAngleSlider.setMaximum(90)
        self.sendSteeringAngleSlider.valueChanged.connect(self.sendSteeringAngleSliderValueChanged)


        # Slider Display labels
        self.sendSteeringAngleLabel = QtWidgets.QLabel("Steering Angle: 0",self)
        self.sendSteeringAngleLabel.setMinimumSize(200,20)
        self.sendSteeringAngleLabel.setAlignment(QtCore.Qt.AlignCenter)

        self.sendThrottleLabel = QtWidgets.QLabel("Throttle: ",self)
        self.sendThrottleLabel.setMinimumSize(200,20)
        self.sendThrottleLabel.setAlignment(QtCore.Qt.AlignCenter)

        # Read State Labels
        self.sentStateLabel = QtWidgets.QLabel("Commands (Click to Send)",self)
        self.sentStateLabel.setMinimumSize(40,20)
        self.sentStateLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.readStateLabel = QtWidgets.QLabel("Received Vehicle Values",self)
        self.readStateLabel.setMinimumSize(40,20)
        self.readStateLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.readBrakeStateLabel = QtWidgets.QLabel("OFF", self)
        self.readBrakeStateLabel.setMinimumSize(40,20)
        self.readBrakeStateLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.readEBrakeStateLabel = QtWidgets.QLabel("OFF", self)
        self.readEBrakeStateLabel.setMinimumSize(40,20)
        self.readEBrakeStateLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.readForksDwnStateLabel = QtWidgets.QLabel("OFF", self)
        self.readForksDwnStateLabel.setMinimumSize(40,20)
        self.readForksDwnStateLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.readForksUpStateLabel = QtWidgets.QLabel(" OFF", self)
        self.readForksUpStateLabel.setMinimumSize(40,20)
        self.readForksUpStateLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.readHornLabel = QtWidgets.QLabel("OFF", self)
        self.readHornLabel.setMinimumSize(40,20)
        self.readHornLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.readSteeringAngle = QtWidgets.QLabel("45", self)
        self.readSteeringAngle.setMinimumSize(40,20)
        self.readSteeringAngle.setAlignment(QtCore.Qt.AlignLeft)

        self.readThrottlePercValues = QtWidgets.QLabel("Throttle Percent: 0", self)
        self.readThrottlePercValues.setMinimumSize(40,20)
        self.readThrottlePercValues.setAlignment(QtCore.Qt.AlignLeft)

        self.readDriveMode = QtWidgets.QLabel("90 Degree Mode",self)
        self.readDriveMode.setMinimumSize(40,20)
        self.readDriveMode.setAlignment(QtCore.Qt.AlignLeft)

        self.readControlMode = QtWidgets.QLabel("MANUAL",self)
        self.readControlMode.setMinimumSize(40,20)
        self.readControlMode.setAlignment(QtCore.Qt.AlignLeft)

        # Graphs
        self.throttleGraph = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100,title = "Throttle Graph", numberLines = 2)
        self.throttleGraph.axes.set_ylim(-100,100)
        self.steeringGraph = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100, title = "Steering Graph", numberLines = 3)
        self.steeringGraph.axes.set_ylim(-90,90)
        
        # Create Line Seperator
        line1 = QtWidgets.QFrame()
        line1.setFrameShape(QtWidgets.QFrame.VLine)
        line1.setFrameShadow(QtWidgets.QFrame.Sunken)

        # Create the controls line sperator
        controlsSeperator1 = QtWidgets.QFrame(self)
        controlsSeperator1.setFrameShape(QtWidgets.QFrame.HLine)
        controlsSeperator1.setFrameShadow(QtWidgets.QFrame.Sunken)

        controlsSeperator2 = QtWidgets.QFrame(self)
        controlsSeperator2.setFrameShape(QtWidgets.QFrame.HLine)
        controlsSeperator2.setFrameShadow(QtWidgets.QFrame.Sunken)

        # Create the Auto file name display, picker button, and the label
        self.autoFileNameLabel = QtWidgets.QLabel("Bag file: ",self)
        self.autoFileNameLabel.setMinimumSize(40,20)
        self.autoFileNameLabel.setAlignment(QtCore.Qt.AlignRight)


        self.autoFileNameTextBox = QtWidgets.QLineEdit(self)
        self.autoFileNameTextBox.setMinimumSize(40,20)
        self.autoFileNameTextBox.setAlignment(QtCore.Qt.AlignLeft)

        self.autofileSelectorButton = QtWidgets.QPushButton("Select Bag",self)
        self.autofileSelectorButton.setMinimumSize(40,20)
        self.autofileSelectorButton.clicked.connect(self.setAutoTestFile)
        
        self.autoTestStartButton = QtWidgets.QPushButton("No Test",self)
        self.autoTestStartButton.setMinimumSize(40,20)
        self.autoTestStartButton.clicked.connect(self.startAutoTest)
        self.autoTestStartButton.setEnabled(False)

        self.autoTestIndicatorLabel = QtWidgets.QLabel("OFF",self)
        self.autoTestIndicatorLabel.setMinimumSize(40,20)
        self.autoTestIndicatorLabel.setAlignment(QtCore.Qt.AlignRight)

        self.logFileNameLabel = QtWidgets.QLabel("Log File: ",self)
        self.logFileNameLabel.setMinimumSize(40,20)
        self.logFileNameLabel.setAlignment(QtCore.Qt.AlignRight)

        self.logFileNameTextBox = QtWidgets.QLineEdit(self)
        self.logFileNameTextBox.setMinimumSize(40,20)
        self.logFileNameTextBox.setAlignment(QtCore.Qt.AlignLeft)

        self.logfileSelectionButton = QtWidgets.QPushButton("Select Log File",self)
        self.logfileSelectionButton.setMinimumSize(40,20)
        self.logfileSelectionButton.clicked.connect(self.selectLogFile)
        
        self.logFileStartButton = QtWidgets.QPushButton("Recording OFF",self)
        self.logFileStartButton.setMinimumSize(40,20)
        self.logFileStartButton.clicked.connect(self.startLogFile)
        self.logFileStartButton.setEnabled(False)

        self.logFileRecordingIndicatorLabel = QtWidgets.QLabel("OFF",self)
        self.logFileRecordingIndicatorLabel.setMinimumSize(40,20)
        self.logFileRecordingIndicatorLabel.setAlignment(QtCore.Qt.AlignRight)

        self.selectCIDirectoryLabel = QtWidgets.QLabel("CI Test Directory: ",self)
        self.selectCIDirectoryLabel.setMinimumSize(40,20)
        self.selectCIDirectoryLabel.setAlignment(QtCore.Qt.AlignRight)

        self.cIDirectoryTextBox = QtWidgets.QLineEdit(self)
        self.cIDirectoryTextBox.setMinimumSize(40,20)
        self.cIDirectoryTextBox.setAlignment(QtCore.Qt.AlignLeft)

        self.ciDirectorySelectButton = QtWidgets.QPushButton("Select Log File",self)
        self.ciDirectorySelectButton.setMinimumSize(40,20)
        self.ciDirectorySelectButton.clicked.connect(self.selectCiDirectory)

        self.runCITestButton = QtWidgets.QPushButton("No Auto Test Selected")
        self.runCITestButton.setMinimumSize(40,20)
        self.runCITestButton.clicked.connect(self.startCITest)
        self.runCITestButton.setEnabled(False)

        self.mainBox = QtWidgets.QHBoxLayout(self.main_widget)
        self.firstBox = QtWidgets.QVBoxLayout()
        self.firstBox.setAlignment(QtCore.Qt.AlignTop)
        self.secondBox = QtWidgets.QGridLayout()
        self.secondBox.setAlignment(QtCore.Qt.AlignTop)


        self.firstBox.addWidget(self.throttleGraph)
        self.firstBox.addWidget(self.steeringGraph)
        
        # First Row
        self.secondBox.addWidget(self.sentStateLabel,0,0)
        self.secondBox.addWidget(self.readStateLabel,0,1)

        # Second Row
        self.secondBox.addWidget(self.sendBrakeStateButton,1,0)
        self.secondBox.addWidget(self.readBrakeStateLabel,1,1)
        
        # Third Row
        self.secondBox.addWidget(self.sendEBrakeStateButton,2,0)
        self.secondBox.addWidget(self.readEBrakeStateLabel,2,1)
        
        # Fourth Row
        self.secondBox.addWidget(self.sendForksUpStateButton,3,0)
        self.secondBox.addWidget(self.readForksUpStateLabel,3,1)
        
        # Fifth row
        self.secondBox.addWidget(self.sendForksDwnStateButton,4,0)
        self.secondBox.addWidget(self.readForksDwnStateLabel,4,1)
        
        # Sixth Row
        self.secondBox.addWidget(self.sendHornStateButton,5,0)
        self.secondBox.addWidget(self.readHornLabel,5,1)

        #Seventh Row
        self.secondBox.addWidget(self.sendSteeringAngleSlider,6,0)
        #Eighth Row
        self.secondBox.addWidget(self.sendSteeringAngleLabel,7,0)
        self.secondBox.addWidget(self.readSteeringAngle,7,1)
        
        #Ninth row
        self.secondBox.addWidget(self.sendThrottleSlider,8,0)
        self.secondBox.addWidget(self.readThrottlePercValues,8,1)
        #Tenth
        self.secondBox.addWidget(self.sendThrottleLabel,9,0)

        #Eleventh
        self.secondBox.addWidget(self.driveModeButton,10,0)
        self.secondBox.addWidget(self.readDriveMode,10,1)


        # Twelth
        self.secondBox.addWidget(self.sendControlMode,11,0)
        self.secondBox.addWidget(self.readControlMode,11,1)

        # Add the seperator
        self.secondBox.addWidget(controlsSeperator1,12,0,1,2)


        #Fourteenth
        self.secondBox.addWidget(self.autoFileNameLabel,13,0)
        self.secondBox.addWidget(self.autoFileNameTextBox,13,1,1,2)
        self.secondBox.addWidget(self.autofileSelectorButton,13,3)

        #Fifteenth
        self.secondBox.addWidget(self.autoTestIndicatorLabel,14,1,1,2)
        self.secondBox.addWidget(self.autoTestStartButton,14,3)
        
        #Sixteenth
        self.secondBox.addWidget(self.logFileNameLabel,15,0)
        self.secondBox.addWidget(self.logFileNameTextBox,15,1,1,2)
        self.secondBox.addWidget(self.logfileSelectionButton,15,3)

        # Seventeenth
        self.secondBox.addWidget(self.logFileRecordingIndicatorLabel,16,1,1,2)
        self.secondBox.addWidget(self.logFileStartButton,16,3)

        # Eighteenth
        self.secondBox.addWidget(self.selectCIDirectoryLabel,17,0)
        self.secondBox.addWidget(self.cIDirectoryTextBox,17,1,1,2)
        self.secondBox.addWidget(self.ciDirectorySelectButton,17,3)

        # Nineteenth
        self.secondBox.addWidget(self.runCITestButton,15,3)

        # Add the layouts
        self.mainBox.addLayout(self.firstBox)
        self.mainBox.addWidget(line1)
        self.mainBox.addLayout(self.secondBox)


        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

    def updateEvent(self):
        """
        Timer Event method that polls the communciationPipe for new data then loads it into the graphs and updates the state
        """
        # get the packet from our communicator
        if (self.commPipe.poll(0)):
            vehData = self.commPipe.recv()
            # Update using the new state
            self.vehicleState.readThrottlePercent = vehData["readThrottlePercent"]
            self.vehicleState.readThrottleVoltage = vehData["readThrottleVoltage"]
            self.vehicleState.readSteeringAngle = vehData["readSteeringAngle"]
            self.vehicleState.readSteeringAVoltage = vehData["readSteeringAVoltage"]
            self.vehicleState.readSteeringBVoltage = vehData["readSteeringBVoltage"]
            self.vehicleState.readFlags = vehData["readFlags"]
            # If the csvWriter is live send it data to record
            if(not self.csvWriterPipe == -1):
                csvWriterDict = {}
                csvWriterDict["goalThrottlePercent"] = self.vehicleState.goalThrottlePercent
                csvWriterDict["goalThrottleVoltage"] = self.vehicleState.goalThrottleVoltage
                csvWriterDict["goalSteeringAngle"] = self.vehicleState.goalSteeringAngle
                csvWriterDict["goalSteeringAVoltage"] = self.vehicleState.goalSteeringAVoltage
                csvWriterDict["goalSteeringBVoltage"] = self.vehicleState.goalSteeringBVoltage
                csvWriterDict["goalFlags"] = self.vehicleState.goalFlags
                csvWriterDict["readThrottlePercent"] = self.vehicleState.readThrottlePercent
                csvWriterDict["readThrottleVoltage"] = self.vehicleState.readThrottleVoltage
                csvWriterDict["readSteeringAngle"] = self.vehicleState.readSteeringAngle
                csvWriterDict["readSteeringAVoltage"] = self.vehicleState.readSteeringAVoltage
                csvWriterDict["readSteeringBVoltage"] = self.vehicleState.readSteeringBVoltage
                csvWriterDict["readFlags"] = self.vehicleState.readFlags
                csvWriterDict["timestamp"] = vehData["timestamps"][len(vehData["timestamps"])-1]
                self.csvWriterPipe.send(csvWriterDict)
            
            # Update the graphs
            self.throttleGraph.pushInValue(vehData["timestamps"]-(self.testStartTime*1000),vehData["throttleData"])
            self.steeringGraph.pushInValue(vehData["timestamps"]-(self.testStartTime*1000),vehData["steeringData"])
            #print("Graph Data Update Took:: " + str(time.time()-start))
            # start = time.time()
            self.throttleGraph.updateFigure()
            self.steeringGraph.updateFigure()
            #print("Graph Redraw Took:: " + str(time.time()-start))
            # start = time.time()
            self.readThrottlePercValues.setText("Throttle Percent: " + str(self.vehicleState.readThrottlePercent) + " %")
            self.readSteeringAngle.setText("Steering Angle: " + str(round(self.vehicleState.readSteeringAngle,1)) + " degrees")
            # Change the status of the live control
            if(not self.autoBagProcess == -1):
                if(not self.autoBagProcess.is_alive()):
                    self.autoTestStartButton.setText("Start Auto Test")
                    self.autoFileNameTextBox.setStyleSheet("background-color: none")
                    self.autoTestIndicatorLabel.setText("OFF")
                    self.autoTestIndicatorLabel.setStyleSheet("background-color: none")

            print("Read Flags:: " +str(self.vehicleState.readFlags))
            print("Fork Down Mask:: " + str(self.vehicleState.Fork_Dwn_Mask))
            print("Fork Up Mask:: " + str(self.vehicleState.Fork_Up_Mask))
            # Update the UI with the new states
            if(self.vehicleState.getBrakeEngaged(readGoalFlags=0)):
                # Engage the brake
                self.readBrakeStateLabel.setText("ON")
                self.readBrakeStateLabel.setStyleSheet("background-color: #baf473")
            else:
                self.readBrakeStateLabel.setText("OFF")
                self.readBrakeStateLabel.setStyleSheet("background-color: none")
            
            if(self.vehicleState.getHornEngaged(readGoalFlags=0)):
                # Engage the horn
                self.readHornLabel.setText("ON")
                self.readHornLabel.setStyleSheet("background-color: #baf473")
            else:
                self.readHornLabel.setText("OFF")
                self.readHornLabel.setStyleSheet("background-color: none")
            
            if(self.vehicleState.getForksDwn(readGoalFlags=0)):
                # Engage the horn
                print("Forks Dwn")
                self.readForksDwnStateLabel.setText("ON")
                self.readForksDwnStateLabel.setStyleSheet("background-color: #baf473")
            else:
                self.readForksDwnStateLabel.setText("OFF")
                self.readForksDwnStateLabel.setStyleSheet("background-color: none")

            if(self.vehicleState.getForksUp(readGoalFlags=0)):
                # Engage the horn
                print("Forks Up")
                self.readForksUpStateLabel.setText("ON")
                self.readForksUpStateLabel.setStyleSheet("background-color: #baf473")
            else:
                self.readForksUpStateLabel.setText("OFF")
                self.readForksUpStateLabel.setStyleSheet("background-color: none")

            if(self.vehicleState.getInSixtyDegreeDriveMode(readGoalFlags=0)):
                # Engage the horn
                self.readDriveMode.setText("60 Degree Mode")
            else:
                self.readDriveMode.setText("90 Degree Mode")


            if(self.vehicleState.getAutoMode(readGoalFlags=0) and not self.vehicleState.getManualMode(readGoalFlags=0) and not self.vehicleState.getEBrakeMode(readGoalFlags=0) and not self.vehicleState.getFailMode(readGoalFlags=0)):
                # Engage the brake
                self.readControlMode.setText("AUTO")
                self.readControlMode.setStyleSheet("background-color: none")
                self.readEBrakeStateLabel.setText("OFF")
                self.readEBrakeStateLabel.setStyleSheet("background-color: none")
            elif(not self.vehicleState.getAutoMode(readGoalFlags=0) and self.vehicleState.getManualMode(readGoalFlags=0) and not self.vehicleState.getEBrakeMode(readGoalFlags=0) and not self.vehicleState.getFailMode(readGoalFlags=0)):
                self.readControlMode.setText("MANUAL")
                self.readControlMode.setStyleSheet("background-color: none")
                self.readEBrakeStateLabel.setText("OFF")
                self.readEBrakeStateLabel.setStyleSheet("background-color: none")
                # Engage the brake
            elif(not self.vehicleState.getAutoMode(readGoalFlags=0) and not self.vehicleState.getManualMode(readGoalFlags=0) and self.vehicleState.getEBrakeMode(readGoalFlags=0) and not self.vehicleState.getFailMode(readGoalFlags=0)):
                self.readControlMode.setText("E-Brake Engaged")
                self.readControlMode.setStyleSheet("background-color: #f49e42")
                self.readEBrakeStateLabel.setText("ON")
                self.readEBrakeStateLabel.setStyleSheet("background-color: #baf473")
                # Engage the brake
            elif(not self.vehicleState.getAutoMode(readGoalFlags=0) and not self.vehicleState.getManualMode(readGoalFlags=0) and not self.vehicleState.getEBrakeMode(readGoalFlags=0) and self.vehicleState.getFailMode(readGoalFlags=0)):
                self.readControlMode.setText("FAIL")
                self.readControlMode.setStyleSheet("background-color: none")
                self.readEBrakeStateLabel.setText("OFF")
                self.readEBrakeStateLabel.setStyleSheet("background-color: none")
                # Engage the brake
            elif(not self.vehicleState.getAutoMode(readGoalFlags=0) and not self.vehicleState.getManualMode(readGoalFlags=0) and not self.vehicleState.getEBrakeMode(readGoalFlags=0) and not self.vehicleState.getFailMode(readGoalFlags=0)):
                self.readControlMode.setText("No Flags Set")
                self.readControlMode.setStyleSheet("background-color: #f49e42")
            else:
                self.readControlMode.setText("ERR- Mutliple Flags set")
                self.readControlMode.setStyleSheet("background-color: #f49e42")
            #print("Button and Text update Redrawing Took:: " + str(time.time()-start))

    def sendData(self):
        """
        Sends the new data states to the HILCommunicatorClass
        """
        # Write this into a socket packet to be sent over to the C-Socket
        #if (self.stateChanged):
        vehSend = {}
        vehSend["goalThrottlePercent"] = self.vehicleState.goalThrottlePercent
        vehSend["goalSteeringAngle"] = self.vehicleState.goalSteeringAngle
        vehSend["goalFlags"] = self.vehicleState.goalFlags
        self.commPipe.send(vehSend)
        self.stateChanged = False
    def savePlots(self):
        """
        TODO:: Implement this functionality
        """
        print("Saving image...")
        pass

    def startAutoTest(self):
        """
        Spawns the new threads for running the selected ros bag in a seperate thread.
        """
        # Spawn a new process that will handle running the test
        if (self.autoBagProcess == -1):
            ctx = mp.get_context('spawn')
            self.autoBagProcess = ctx.Process(target=RunAutoTest,args=(self.autoFileNameTextBox.text(),))
            self.autoTestStartButton.setText("Stop Auto Test")
            self.autoFileNameTextBox.setStyleSheet("background-color: #f49e42")
            self.autoTestIndicatorLabel.setText("LIVE")
            self.autoTestIndicatorLabel.setStyleSheet("background-color: red")
            self.autoBagProcess.start()
        else:
            if (self.autoBagProcess.is_alive()):
                self.autoBagProcess.terminate()
            # Reset to be run again
            self.autoBagProcess = -1
            self.autoTestStartButton.setText("Start Auto Test")
            self.autoFileNameTextBox.setStyleSheet("background-color: none")
            self.autoTestIndicatorLabel.setText("OFF")
            self.autoTestIndicatorLabel.setStyleSheet("background-color: none")

    def setAutoTestFile(self):    
        """
        Sets the Auto Test File textbox to the selected file name if it is a ros bag file
        """
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,"Select a ros .bag file", "","All Files (*);;Ros Bag Files (*.bag)", options=options)
        if (fileName.find(".bag") == -1 ):
            QtWidgets.QMessageBox.question(self, 'Error',
                                            "This is an invalid file format. Select a '.bag' file.",
                                            QtWidgets.QMessageBox.Yes)
        else:
            self.autoFileNameTextBox.setText(fileName)
            self.autoTestStartButton.setText("RUN")
            self.autoTestStartButton.setEnabled(True)
    def startCITest(self):
        """
        TODO: Fix and Implement this functionality
        """
        if(self.ciTestProcess == -1):
            # Start the process
            print("Starting the CI Process and Shutting GUI Off")
            # Pass the pipe onto another thread
            #ctx = mp.get_context('spawn')
            #self.ciTestProcess = ctx.Process(target=CiTestProcess,args=(self.cIDirectoryTextBox.text(),self.logFileNameTextBox.text(),self.communicationPipe,))
            #self.ciTestProcess.run()
            # Busy wait until it is ready to die
            #while(not mp.is_alive(self.ciTestProcess)):
             #   pass

            # Close the window once we have the process set up
            #self.close()
    def selectCiDirectory(self):
        """
        Selects the CI Directory to save tests in or write new tests to
        """
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,"Select the directory to run all the tests out of", "","All Files (*);;Ros Bag Files (*.bag)", options=options)
        if (fileName.find(".bag") == -1 ):
            QtWidgets.QMessageBox.question(self, 'Error',
                                            "This is an invalid file format. Select a '.bag' file.",
                                            QtWidgets.QMessageBox.Yes)
        else:
            self.cIDirectoryTextBox.setText(fileName)
            self.runCITestButton.setText("RUN CI Test")
            self.runCITestButton.setEnabled(True)

    def startLogFile(self):
        """
        Start or stop the logging threads.
        """
        if (self.logDataProcess == -1):
            killRecordingFlag_.value = 1
            ctx = mp.get_context('spawn')
            self.csvWriterPipe,self.csvReceiverPipe = mp.Pipe()
            self.logDataProcess = ctx.Process(target=DataWriter,args=(self.logFileNameTextBox.text(),self.csvReceiverPipe,))
            self.logFileStartButton.setText("STOP")
            self.logFileNameTextBox.setStyleSheet("background-color: #f49e42")
            self.logFileRecordingIndicatorLabel.setText("RECORDING")
            self.logFileRecordingIndicatorLabel.setStyleSheet("background-color: red")
            self.logDataProcess.start()
        else:
            killRecordingFlag_.value = 0
            self.csvWriterPipe.close()

            self.csvWriterPipe = -1
            # Reset to be run again
            self.logDataProcess = -1
            self.logFileStartButton.setText("RECORD")
            self.logFileNameTextBox.setStyleSheet("background-color: none")
            self.logFileRecordingIndicatorLabel.setText("OFF")
            self.logFileRecordingIndicatorLabel.setStyleSheet("background-color: none")

    def selectLogFile(self):
        """
        Select the log file to save data to
        """
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,"Select a log file", "","All Files (*);;Ros Bag Files (*.bag)")
        if(fileName.find(".csv") == -1):
            QtWidgets.QMessageBox.question(self, 'Error',
                                            "This is an invalid file format. Select a '.log' file.",
                                            QtWidgets.QMessageBox.Yes)
        else:
            self.logFileNameTextBox.setText(fileName)
            self.logFileStartButton.setText("RECORD")
            self.logFileStartButton.setEnabled(True)

    def sendNewBrakeState(self):
        """
        Update the brake state button
        """
        if (self.vehicleState.getBrakeEngaged(readGoalFlags=1)):
            # flip to off
            self.vehicleState.setBrakeEngaged(False)
            self.sendBrakeStateButton.setStyleSheet("background-color: None")
            self.sendBrakeStateButton.setText("Brake set to  OFF")
        else:
            # flip to on
            self.vehicleState.setBrakeEngaged(True)
            self.sendBrakeStateButton.setStyleSheet("background-color: #f49e42")
            self.sendBrakeStateButton.setText("Brake set to  ON")
        self.stateChanged = True
        
    def sendNewEBrakeState(self):
        """
        Update the new E-Brake State
        """
        if (self.vehicleState.getEBrakeMode(readGoalFlags=1)):
            # flip to off
            self.vehicleState.setEBrakeMode(False)
            self.sendEBrakeStateButton.setStyleSheet("background-color: None")
            self.sendEBrakeStateButton.setText("E-Brake set to  OFF")
        else:
            # flip to on
            self.vehicleState.setEBrakeMode(True)
            self.sendEBrakeStateButton.setStyleSheet("background-color: #f49e42")
            self.sendEBrakeStateButton.setText("E-Brake set to  ON")
        self.stateChanged = True
        
    def sendNewForksDwnState(self):
        """
        Update the new Forks Dwn State
        """
        if (self.vehicleState.getForksDwn(readGoalFlags=1)):
            # flip to off
            self.vehicleState.setForksDwn(False)
            self.sendForksDwnStateButton.setStyleSheet("background-color: None")
            self.sendForksDwnStateButton.setText("Forks Dwn set to  OFF")
        else:
            # flip to on
            self.vehicleState.setForksDwn(True)
            self.sendForksDwnStateButton.setStyleSheet("background-color: #f49e42")
            self.sendForksDwnStateButton.setText("Forks Dwn set to  ON")
        self.stateChanged = True
        #self.show()

    def sendNewForksUpState(self):
        """
        Update the new Forks Up State
        """
        if (self.vehicleState.getForksUp(readGoalFlags=1)):
            # flip to Off
            self.vehicleState.setForksUp(False)
            self.sendForksUpStateButton.setStyleSheet("background-color: None")
            self.sendForksUpStateButton.setText("Forks Up set to  OFF")
        else:
            # flip to on
            self.vehicleState.setForksUp(True)
            self.sendForksUpStateButton.setStyleSheet("background-color: #f49e42")
            self.sendForksUpStateButton.setText("Forks Up set to  ON")
        self.stateChanged = True
       
    def sendNewHornState(self):
        """
        Update the new Horn State
        """
        if (self.vehicleState.getHornEngaged(readGoalFlags=1)):
            # flip to off
            self.vehicleState.setHornEngaged(False)
            self.sendHornStateButton.setStyleSheet("background-color: None")
            self.sendHornStateButton.setText("Horn set to  OFF")
        else:
            # flip to on
            self.vehicleState.setHornEngaged(True)
            self.sendHornStateButton.setStyleSheet("background-color: #f49e42")
            self.sendHornStateButton.setText("Horn set to  ON")   
        self.stateChanged = True
    
    def throttleSliderValueChanged(self):
        """
        Update the new throttle slider percent
        """
        self.sendThrottleLabel.setText("Throttle: " + str(self.sendThrottleSlider.sliderPosition()))
        self.vehicleState.setGoalThrottle(self.sendThrottleSlider.sliderPosition())
        self.stateChanged = True

    def sendSteeringAngleSliderValueChanged(self):
        """
        Update the new steering angle slider
        """
        self.sendSteeringAngleLabel.setText("Steering Angle: " +str(self.sendSteeringAngleSlider.sliderPosition()))
        self.vehicleState.setGoalSteeringAngle(self.sendSteeringAngleSlider.sliderPosition())
        self.stateChanged = True

    def toggleControlMode(self):
        """
        Update the control mode
        """
        if (self.vehicleState.getAutoMode(readGoalFlags=1)):
            # Toggle from AUTO --> Manual
            self.vehicleState.setManualMode(True)
            self.vehicleState.setAutoMode(False)
            self.sendControlMode.setText("IN MANUAL MODE")
            self.sendControlMode.setStyleSheet("background-color: #baf473")
            # Probably should send an update here as well
        else:
            # Toggle form MANUAL --> AUTO
            self.vehicleState.setManualMode(False)
            self.vehicleState.setAutoMode(True)
            self.sendControlMode.setText("IN AUTO MODE")
            self.sendControlMode.setStyleSheet("background-color: #f49e42")
        self.stateChanged = True

    def toggleDriveMode(self):
        """
        Update the new drive mode
        """
        if (self.vehicleState.getInSixtyDegreeDriveMode(readGoalFlags=1)):
            # set to 90 degree mode
            self.vehicleState.setInSixtyDegreeDriveMode(False)
            self.driveModeButton.setText("Speed Mode set to 90 Degree Mode")
            self.driveModeButton.setStyleSheet("background-color: #baf473")
        else:
            # set to 60 degree mode
            self.vehicleState.setInSixtyDegreeDriveMode(True)
            self.driveModeButton.setText("Speed Mode set to 60 Degree Mode")
            self.driveModeButton.setStyleSheet("background-color: #f49e42")
        self.stateChanged = True

    def fileQuit(self):
        """
        Exit the application
        """
        self.close()

    def closeEvent(self, ce):
        """
        Handle a close event
        """
        self.fileQuit()


    def about(self):
        """
        Glib about page message
        """
        QtWidgets.QMessageBox.about(self, "About","Gui for the HIL.. What the hell do you think this is?")

def GuiProcess(pipeConnection):
    """
    Process for running the HIL GUI
    """
    qApp = QtWidgets.QApplication(sys.argv)

    aw = HilGuiWindow(pipeConnection)
    aw.setWindowTitle("Stocked Robotics HIL Suite")
    aw.show()
    qApp.exec_()

def CommunicationProcess(pipeConnection):
    """
    Process for running the Communciation pipes
    """
    # Function for writing the 
    comms = HilCommunicatorClass(pipeConnection)
    while(killCommunicationFlag_.value == 1):
        comms.readDataFromSocket()
        comms.recvPacketFromGui()

# TODO: Implement
def CiTestProcess(autoFileName,logFileName,communicationPipe):
    pass

def RunAutoTest(fileName):
    """
    Run the Autonomous test
    """
    # TODO:: Find out the proper command to go here
    subprocess.run(["rosbag play "+fileName], shell=True, check=True)
    #print(fileName)



def DataWriter(logFilePath,communicationPipe):
    """
    Data Write to write incoming data to a CSV file

    Args:
        logFilePath: path to the file to log to
        communicationPipe: Pipe to receive data on
    """
    csvFile = open(logFilePath,"w")
    fieldnames = ""
    fieldnames += "goalThrottlePercent,"
    fieldnames += "goalThrottleVoltage,"
    fieldnames += "goalSteeringAngle,"
    fieldnames += "goalSteeringAVoltage,"
    fieldnames += "goalSteeringBVoltage,"
    fieldnames += "goalFlags,"
    fieldnames += "readThrottlePercent,"
    fieldnames += "readThrottleVoltage,"
    fieldnames += "readSteeringAngle,"
    fieldnames += "readSteeringAVoltage,"
    fieldnames += "readSteeringBVoltage,"
    fieldnames += "readFlags,"
    fieldnames += "timestamp,"
    fieldnames += "\n"
    csvFile.write(fieldnames)
    while(killRecordingFlag_.value == 1):
        if (communicationPipe.poll(0)):
            try:
                vehData = communicationPipe.recv()
            except:
                # The pipe has been close so we break out of the loop and go home
                break
            values = ""
            values += str(vehData["goalThrottlePercent"]) + ","
            values += str(vehData["goalThrottleVoltage"]) + ","
            values += str(vehData["goalSteeringAngle"]) + ","
            values += str(vehData["goalSteeringAVoltage"]) + ","
            values += str(vehData["goalSteeringBVoltage"]) + ","
            values += str(vehData["goalFlags"]) + ","
            values += str(vehData["readThrottlePercent"]) + ","
            values += str(vehData["readThrottleVoltage"]) + ","
            values += str(vehData["readSteeringAngle"]) + ","
            values += str(vehData["readSteeringAVoltage"]) + ","
            values += str(vehData["readSteeringBVoltage"]) + ","
            values += str(vehData["readFlags"]) + ","
            values += str(vehData["timestamp"]) + ",\n"
            csvFile.write(values)
    csvFile.close()

if (__name__ == "__main__"):
    """
    Main process
    """
    # Setup system pipes and processes
    socketPipe,guiPipe = mp.Pipe() # Create the pipes
    guiProc = mp.Process(target=GuiProcess,args=(guiPipe,)) # Start the GUI
    sockProc = mp.Process(target=CommunicationProcess,args=(socketPipe,)) # start the hil socket process
    guiProc.start() # start the GUI process
    sockProc.start() # start the socket process
    guiProc.join() # Wait for the HIL_GUI to exit before dying
    killCommunicationFlag_.value = 0 # Once the HIL GUI has exiting kill the socket process
    if (killRecordingFlag_.value == 1):
        killRecordingFlag_.value = 0
    sockProc.join() # wait for the process to die
