"""
Title:: gui_comms_test_server.py
Brief:: Implementation of a basic server that mimics the hil to send/receive data. This is meant to test the update functionality of the hil
Author:: Nicholas Hemstreet
Copyright:: Stocked Robotics, All rights reserved
"""


import socket as sock
import time
import struct

class FirmwareToGUIPacket(object):
    """docstring for FirmwareToGUIPacket"""
    def __init__(self,forkliftType="Crown_PC4500"):
        super(FirmwareToGUIPacket, self).__init__()
        
        self.throttlePercentToVelocityGain = 0.0
        self.throttlePercentToVelocityOffset = 0.0
        self.throttlePercentToVoltageGain = 0.0
        if (forkliftType == "Crown_PC4500"):
            # Ratio taken from the firmware, needs to be updated based on the schematic
            self.throttlePercentToVelocityGain = 1.0/0.5
            # Offset taken form the firmware, needs to be updated based on the mapping
            self.throttlePercentToVelocityOffset = -20/0.5
            # Offset taken form the firmware, needs to be updated based on the mapping
            self.throttlePercentToVoltageGain = 12.0/100.0
            # Offset taken form the firmware, needs to be updated based on the mapping
            self.steeringNeutralVoltage = 2.50
            self.steeringDeltaVoltage = 1.50
        elif (forkliftType == "Raymond_8510"):
            self.throttlePercentToVelocityGain = 1.0/0.45
            self.throttlePercentToVelocityOffset = -10.0/0.45
            self.throttlePercentToVoltageGain = 12.0/100.0
            self.steeringNeutralVoltage = 3.5
            self.steeringDeltaVoltage = 0.5
        # There is no default case, we will just keep the above values
        self.goalThrottleVelocity = 0
        self.goalThrottleVoltage = 0
        self.goalThrottlePercent = 0
        self.goalSteeringAngle = 0
        self.goalSteeringAVoltage = 0
        self.goalSteeringBVoltage = 0
        self.goalFlags = 0
        self.readThrottleVelocity = 0
        self.readThrottlePercent = 0
        self.readThrottleVoltage = 0
        self.readSteeringAngle = 0
        self.readSteeringAVoltage = 0
        self.readSteeringBVoltage = 0
        self.readFlags = 0
        self.timestamp = 0

        self.timestamp = 0.0
        # Flag Masks
        self.BrakeMask = 0b0000000000000001
        self.HornMask  = 0b0000000000000010
        self.Fork_Up_Mask = 0b0000000000000100
        self.Fork_Dwn_Mask = 0b0000000000001000
        self.Auto_Mode_Mask = 0b0000000100000000
        self.Manual_Mode_Mask = 0b0000001000000000
        self.EBrake_Mode_Mask = 0b0000010000000000
        self.Fail_Mode_Mask =   0b0000100000000000
        self.Sixty_Degree_Mode_Mask =  0b0001000000000000

    def getBrakeEngaged(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.BrakeMask) == self.BrakeMask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.BrakeMask) == self.BrakeMask):
                return True
            else:
                return False
    def getHornEngaged(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.HornMask) == self.HornMask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.HornMask) == self.HornMask):
                return True
            else:
                return False
    def getForksUp(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.Fork_Up_Mask) == self.Fork_Up_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Fork_Up_Mask) == self.Fork_Up_Mask):
                return True
            else:
                return False
    def getForksDwn(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.Fork_Dwn_Mask) == self.Fork_Dwn_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Fork_Dwn_Mask) == self.Fork_Dwn_Mask):
                return True
            else:
                return False
    def getAutoMode(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.Auto_Mode_Mask) == self.Auto_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Auto_Mode_Mask) == self.Auto_Mode_Mask):
                return True
            else:
                return False
    def getManualMode(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.Manual_Mode_Mask) == self.Manual_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Manual_Mode_Mask) == self.Manual_Mode_Mask):
                return True
            else:
                return False
    def getEBrakeMode(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.EBrake_Mode_Mask) == self.EBrake_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.EBrake_Mode_Mask) == self.EBrake_Mode_Mask):
                return True
            else:
                return False
    def getFailMode(self,readGoalFlags = 0):
        if (readGoalFlags):
            if((self.goalFlags & self.Fail_Mode_Mask) == self.Fail_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Fail_Mode_Mask) == self.Fail_Mode_Mask):
                return True
            else:
                return False
    def getInSixtyDegreeDriveMode(self,readGoalFlags = 0):
        
        if (readGoalFlags):
            if((self.goalFlags & self.Sixty_Degree_Mode_Mask) == self.Sixty_Degree_Mode_Mask):
                return True
            else:
                return False
        else:    
            if((self.readFlags & self.Sixty_Degree_Mode_Mask) == self.Sixty_Degree_Mode_Mask):
                return True
            else:
                return False
    
    def setLatestTimestamp(self,timestamp):
        self.timestamp = timestamp

    def getLatestTimeStamp(self):
        return self.timestamp
    
    def setReadFlags(self):
        self.readFlags = readFlags

    def setGoalThrottle(self,percent):
        self.goalThrottlePercent = percent
        self.goalThrottleVelocity = ((self.throttlePercentToVelocityGain*percent) - self.throttlePercentToVelocityOffset)/100.0
        print(str(((self.throttlePercentToVelocityGain*percent) - self.throttlePercentToVelocityOffset)/100.0))
        self.goalThrottleVoltage = (self.throttlePercentToVoltageGain*percent)

    def setGoalSteeringAngle(self,angle):
        self.goalSteeringAngle = angle
        self.goalSteeringAVoltage = self.steeringNeutralVoltage - (self.steeringDeltaVoltage*angle/90.0)
        self.goalSteeringBVoltage = self.steeringNeutralVoltage + (self.steeringDeltaVoltage*angle/90.0)
    
    def setBrakeEngaged(self,engaged):
        if (engaged):
            self.goalFlags |= self.BrakeMask
        else:
            self.goalFlags -= ((self.BrakeMask)*((self.goalFlags & self.BrakeMask) == self.BrakeMask))

    def setHornEngaged(self,engaged):
        if (engaged):
            self.goalFlags |= self.HornMask
        else:
            self.goalFlags -= ((self.HornMask)*((self.goalFlags & self.HornMask) == self.HornMask))
    
    def setForksUp(self,engaged):
        if (engaged):
            self.goalFlags |= self.Fork_Up_Mask
        else:
            self.goalFlags -= ((self.Fork_Up_Mask)*((self.goalFlags & self.Fork_Up_Mask) == self.Fork_Up_Mask))
    
    def setForksDwn(self,engaged):
        if (engaged):
            self.goalFlags |= self.Fork_Dwn_Mask
        else:
            self.goalFlags -= ((self.Fork_Dwn_Mask)*((self.goalFlags & self.Fork_Dwn_Mask) == self.Fork_Dwn_Mask))
    
    def setAutoMode(self,engaged):
        if (engaged):
            self.goalFlags |= self.Auto_Mode_Mask
        else:
            self.goalFlags -= ((self.Auto_Mode_Mask)*((self.goalFlags & self.Auto_Mode_Mask) == self.Auto_Mode_Mask))

    def setManualMode(self,engaged):
        if (engaged):
            self.goalFlags |= self.Manual_Mode_Mask
        else:
            self.goalFlags -= ((self.Manual_Mode_Mask)*((self.goalFlags & self.Manual_Mode_Mask) == self.Manual_Mode_Mask))

    def setEBrakeMode(self,engaged):
        if (engaged):
            self.goalFlags |= self.EBrake_Mode_Mask
        else:
            self.goalFlags -= ((self.EBrake_Mode_Mask)*((self.goalFlags & self.EBrake_Mode_Mask) == self.EBrake_Mode_Mask))
    
    def setFailMode(self,engaged):
        if (engaged):
            self.goalFlags |= self.Fail_Mode_Mask
        else:
            self.goalFlags -= ((self.Fail_Mode_Mask)*((self.goalFlags & self.Fail_Mode_Mask) == self.Fail_Mode_Mask))
    
    def setInSixtyDegreeDriveMode(self,engaged):
        if (engaged):
            self.goalFlags |= self.Sixty_Degree_Mode_Mask
        else:
            self.goalFlags -= ((self.Sixty_Degree_Mode_Mask)*((self.goalFlags & self.Sixty_Degree_Mode_Mask) == self.Sixty_Degree_Mode_Mask))

    def getStateInByteFormat(self):
        """
        Return the format of the vehicle state in a byte string
        """
        pack = b""
        stamp = time.time() *1000 # Get the Epoch in milliseconds
        # Extract the useful bits
        
        pack += struct.pack("BBBBB",0xFF,0xFF,0xFF,0xFF,0xFF)
        pack += struct.pack("h",int(self.goalThrottleVelocity*100))
        pack += struct.pack("h",int(self.goalThrottleVoltage*100))
        pack += struct.pack("h",int(self.goalThrottlePercent))
        pack += struct.pack("h",int(self.goalSteeringAngle))
        pack += struct.pack("h",int(self.goalSteeringAVoltage))
        pack += struct.pack("h",int(self.goalSteeringBVoltage))
        pack += struct.pack("H",int(self.goalFlags))
        pack += struct.pack("h",int(self.readThrottleVelocity*100))
        pack += struct.pack("h",int(self.readThrottlePercent))
        pack += struct.pack("h",int(self.readThrottleVoltage*100))
        pack += struct.pack("h",int(self.readSteeringAngle*(157/90.0)))
        pack += struct.pack("h",int(self.readSteeringAVoltage*100))
        pack += struct.pack("h",int(self.readSteeringBVoltage*100))
        pack += struct.pack("H",int(self.readFlags))
        pack += struct.pack("l",int(stamp))
        return pack




if (__name__ == "__main__"):
    vehicleState = FirmwareToGUIPacket("Crown_PC4500")
    socket = sock.socket(sock.AF_INET,sock.SOCK_DGRAM)
    socket.bind(("127.0.0.1",1236))
    connected = False
    while(1):
        start = time.time()
        if (not connected):
            try: 
                socket.connect(("127.0.0.1",5666))
                connected = True
            except:
                connected = False
        if (connected):
            # Logic for seeing an increase/decrease in throttle or steering angle
            if (vehicleState.goalThrottlePercent < 100):
                vehicleState.setGoalThrottle(vehicleState.goalThrottlePercent+1)
            else:
                vehicleState.setGoalThrottle(0)
            
            if (vehicleState.goalSteeringAngle < 90):
                vehicleState.setGoalSteeringAngle(vehicleState.goalSteeringAngle+1)
            else:
                vehicleState.setGoalSteeringAngle(-90)
            # Pass over state for our cheat
            vehicleState.readThrottlePercent = vehicleState.goalThrottlePercent
            vehicleState.readThrottleVelocity = vehicleState.goalThrottleVelocity
            vehicleState.readThrottleVoltage = vehicleState.goalThrottleVoltage

            vehicleState.readSteeringAngle = vehicleState.goalSteeringAngle
            vehicleState.readSteeringAVoltage = vehicleState.goalSteeringAVoltage
            vehicleState.readSteeringBVoltage = vehicleState.goalSteeringBVoltage
            print(str(vehicleState.readThrottleVelocity))
            # Setup our flags like we want
            vehicleState.setBrakeEngaged(True)

            vehicleState.setHornEngaged(True)

            vehicleState.setManualMode(True)

            # Spoof like we are the connected vehicle
            vehicleState.readFlags = vehicleState.goalFlags

            byteArray = vehicleState.getStateInByteFormat()
            try:
                socket.sendto(byteArray,("127.0.0.1",5666))
            except:
                connected = False
            time.sleep(0.1)


            