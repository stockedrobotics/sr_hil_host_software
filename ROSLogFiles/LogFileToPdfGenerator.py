"""
Title:: LogFileToPdfGenerator.py
Brief:: Converts log files of the standard stocked robotics form to pdfs
Author:: Nicholas Hemstreet
"""
import os
import sys
import matplotlib
matplotlib.use('Qt5Agg')
from PyQt5.QtWidgets import QApplication 
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtPrintSupport import QPrinter
from PyQt5.QtCore import  QUrl
from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


class LogFileParser(object):
    """docstring for LogFileParser"""
    def __init__(self,directoryForOutput=""):
        super(LogFileParser, self).__init__()
        
        # Make sure we are specifying a real directory
        if (not self.directoryForOutput[len(self.directoryForOutput-1)] == '/'):
            self.directoryForOutput += "/"

        if (os.path.exists(directoryForOutput)):
            print("Using:: " + directoryForOutput " for output")
            self.directoryForOutput = directoryForOutput
        else:
            print(directoryForOutput " is invalid.")
            print("Using:: ./output_files/")
            self.directoryForOutput = "./output_files/"
            if (not os.path.exists(self.directoryForOutput)):
                print("./output_files not found. Creating now...")
                # make the directory
                os.mkdir(self.directoryForOutput)

    def generateHTMLForLogFile(self,logFilePath,destFileName):
        if(not os.path.exists(logFilePath))
            print("An invalid logFilePath: " + logFilePath + " was specified.")
            return 0
        if (not destFileName.find(".html") == -1):
            print("Invalid file name.")
            print("Just send the name with no file extension")
            return 0

        # Open the file
        logFile = open(logFilePath,"r")

        # Create the destination folder
        if (not os.path.exists(self.directoryForOutput + destFileName)):
            os.mkdir(self.directoryForOutput+destFileName)

        # Create the destination file
        htmlFile = open(self.directoryForOutput + destFileName + "/" + destFileName + ".html","w")

        htmlFile.write("<header><h1>" + logFilePath + "</h1</header><body>")
        logFileDictionary = {"mission_ids":[],"failures":0,"successes":0}
        headerLevel = 2 # For every nested value we will increase our header one
        for line in logFile:
            # Parse each line into the log file
            if (not line.find("\t") == -1):
                # Found header
            elif (line.find("\t") == -1 ):
                # Found a data item
                variableName = line[line.find("\t")+1:line.find(":")]
                if (not variableName.find("~") == -1):
                    # Found a graph
                else:
                    # Found a variable
                    variableValue = line[line.find(":")+1:]
                    htmlFile.write("<table><tr>")
    def createDirectoryForOutput(self,directoryName):
        pass


class PdfGenerator(object):
    """docstring for PDFGenerator"""
    def __init__(self, outputDirectory=""):
        super(PDFGenerator, self).__init__()
        self.outputDirectory = outputDirectory

    def createPdfForHtmlFile(self,htmlFile,inputFileDirectory,outputFileName=""):
        
        if (outputFileName == ""):
            outputFileName = htmlFile.replace(".html",".pdf")



        
